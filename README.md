# test-tech-pricing-hub



## Description

Ce projet a pour but de calculer la variation de prix de chaque produit entre la  première et la dernière date présente dans le fichier (option A) ou les deux dernières dates connues de chaque produit fourni par le fichier (option B)

Le type de calcul est un paramètre de l'appel au service.


Exemple si un produit  A a 3 points de données

01/01/2021 10.00

01/02/2021 20.00

01/03/2021 25.00

La fonction doit retourner

A 5.00 ( soit la variation entre les 2 dernières dates)

ou 

A = 15 si on demande la variation entre le premier et le dernier prix.



## Requirements
- il faut avoir Flask installé
```
pip install flask

```
- Ce projet est codé en Python

## Utilisation de l'API 

la requete curl doit contenir les informations suivantes : 
- skus : liste des id produits séparés par des "_"
- option : A ou B 
    - option A : la variation de prix entre la  première et la dernière date connues
    - options B : la variation de prix de chaque produit entre les deux dernières dates connues

## Exemple requete : 

```
curl "http://localhost:5000/user?skus=100206_100486_100495_15655&option=A"

```
