import pandas as pd
from flask import Flask, request

app = Flask(__name__)

#Read file data.csv
df = pd.read_csv("data.csv", header=0, sep=';')

#convert list of skus to df 
def prepare_filtred_df(input, df):
    liste = list(input.split("_"))
    liste = map(int, liste)
    sorted_df = df[df['product'].isin(liste)]
    return sorted_df

# Option A : diff between min and max dates
def option_one(df):
    df_one = df[
        df.groupby('product')['activity_date'].transform(lambda x: x.eq(x.max()) | x.eq(x.min()))].sort_values(
        'price')
    return df_one

#option B : diff between two most recent dates
def option_two(df):
    df_two = df.sort_values('activity_date', ascending=False).groupby('product').head(2).sort_values('price')
    return df_two

#calculate diff between two selected dates
def diff_calcul(df):
    df['dif'] = df.groupby('product')['price'].diff(-1) * (-1)
    df = df[df['dif'].notna()]
    df = df[['product', 'dif']]
    return df

#Welcome page
@app.route('/', methods=['GET'])
def hello():
    if request.method == 'GET':
        data = "Hello, please enter skus and option A or B , " \
               "exp : curl 'http://localhost:5000/user?skus=100206_100486_100495_15655&option=A'"
        return data


@app.route('/user')
def get_user_details():
    skus = request.args.get('skus')
    if skus == "":
        return "you need at least one sku"
    option = request.args.get('option').strip()
    if option not in ("A", "B"):
        return "you need to choose option A or B"
    try:
        traited_skus = prepare_filtred_df(skus, df)
    except:
        return "please recheck the skus"
    if option == "A":
        df_organized = option_one(traited_skus)
    elif option == "B":
        df_organized = option_two(traited_skus)

    final_df = diff_calcul(df_organized)
    result = final_df.reset_index(drop=True).to_json(orient='records')
    return result


if __name__ == '__main__':
    app.run()
